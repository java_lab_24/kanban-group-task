package com.pattern_two;

import com.pattern_two.controller.MainController;

public class Run {
    public static void main(String[] args) {
        MainController mainController = new MainController();
        mainController.run();
    }
}