package com.pattern_two.model;

public class Admin {

    private String name;
    private TasksHolder tasksHolder;

    public Admin(String name, TasksHolder tasksHolder) {
        this.name = name;
        this.tasksHolder = tasksHolder;
    }

    public void addInToDo(Task task){
        tasksHolder.getTasks().add(task);
    }

    public void addInDone(Task task){
        try {
            task.getTaskDeveloper().setTask(null);
            task.getTaskDeveloper().getFinishedTasks().add(task);
        }catch (NullPointerException e){
            System.out.println("This task didn't take any developer");
        }
        task.addInDone();
    }
    public void addInBlocked(Task task){
        try {
            task.getTaskDeveloper().setTask(null);
            task.setTaskDeveloper(null);
        }catch (NullPointerException e){
            System.out.println("This task didn't take any developer");
        }
        task.addInBlocked();
    }
    public void returnInToDo(Task task){
        try {
            task.getTaskDeveloper().setTask(null);
            task.setTaskDeveloper(null);
        }catch (NullPointerException e){
            System.out.println("This task didn't take any developer");
        }
        task.setTaskDeveloper(null);
        task.addInToDo();
    }

    public String getName() {
        return name;
    }
}
