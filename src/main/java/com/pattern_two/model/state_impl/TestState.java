package com.pattern_two.model.state_impl;

import com.pattern_two.model.State;
import com.pattern_two.model.Task;
import com.pattern_two.model.state_impl.BlockedState;
import com.pattern_two.model.state_impl.ProgressState;
import com.pattern_two.model.state_impl.ToDoState;
import com.pattern_two.model.state_impl.VerificationState;

public class TestState implements State {

    @Override
    public void addInToDo(Task task) {
        task.setTaskState(new ToDoState());
        System.out.println("Added In ToDo!");
    }
    @Override
    public void addInProgress(Task task) {
        task.setTaskState(new ProgressState());
        System.out.println("Added In Progress!");
    }
    @Override
    public void addInVerification(Task task) {
        task.setTaskState(new VerificationState());
        System.out.println("Added In Verification!");
    }
    @Override
    public void addInBlocked(Task task) {
        task.setTaskState(new BlockedState());
        System.out.println("Added In Blocked!");
    }

    @Override
    public String getStateName(){
        return "TestState";
    }
}

