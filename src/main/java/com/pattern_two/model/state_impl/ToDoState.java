package com.pattern_two.model.state_impl;

import com.pattern_two.model.State;
import com.pattern_two.model.Task;
import com.pattern_two.model.state_impl.ProgressState;

public class ToDoState implements State {

    @Override
    public void addInProgress(Task task) {
        task.setTaskState(new ProgressState());
        System.out.println("Added In Progress!");
    }

    @Override
    public String getStateName(){
        return "ToDoState";
    }
}
