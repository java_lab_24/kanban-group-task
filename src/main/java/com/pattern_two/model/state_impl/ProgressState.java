package com.pattern_two.model.state_impl;

import com.pattern_two.model.State;
import com.pattern_two.model.Task;
import com.pattern_two.model.state_impl.BlockedState;
import com.pattern_two.model.state_impl.CodeReviewState;

public class ProgressState implements State {

    @Override
    public void addInCodeReview(Task task) {
        task.setTaskState(new CodeReviewState());
        System.out.println("Added In Code Review!");
    }

    @Override
    public void addInBlocked(Task task) {
        task.setTaskState(new BlockedState());
        System.out.println("Added In Blocked!");
    }

    @Override
    public String getStateName(){
        return "ProgressState";
    }
}
